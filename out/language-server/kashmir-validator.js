"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KashmirValidator = exports.registerValidationChecks = void 0;
/**
 * Register custom validation checks.
 */
function registerValidationChecks(services) {
    const registry = services.validation.ValidationRegistry;
    const validator = services.validation.KashmirValidator;
    const checks = {};
    registry.register(checks, validator);
}
exports.registerValidationChecks = registerValidationChecks;
/**
 * Implementation of custom validations.
 */
class KashmirValidator {
}
exports.KashmirValidator = KashmirValidator;
//# sourceMappingURL=kashmir-validator.js.map