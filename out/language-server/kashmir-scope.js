"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomScopeComputation = void 0;
const langium_1 = require("langium");
const ast_1 = require("./generated/ast");
/**
 * Gerador customizado para o escopo global do arquivo.
 * Por padrão, o escopo global só contém os filhos do nó raiz,
 * sejam acessíveis globalmente
 */
class CustomScopeComputation extends langium_1.DefaultScopeComputation {
    computeExports(document, cancelToken) {
        const _super = Object.create(null, {
            computeExports: { get: () => super.computeExports }
        });
        return __awaiter(this, void 0, void 0, function* () {
            // Os nós que normalmente estariam no escopo global
            const default_global = yield _super.computeExports.call(this, document, cancelToken);
            const root = document.parseResult.value;
            // Colocar no escopo global todas as LocalEntity de todos os Module com o nome normal
            const entities = root.abstractElements.filter(ast_1.isModule).flatMap(m => m.elements.filter(ast_1.isLocalEntity).map(e => this.descriptions.createDescription(e, `${e.$container.name}.${e.name}`, document)));
            root.abstractElements.filter(ast_1.isModule).flatMap(m => m.elements.filter(ast_1.isLocalEntity).map(e => this.exportNode(e, default_global, document)));
            // Colocar no escopo global todas as LocalEntity de todos os Module com o nome normal
            const entities_imported = root.abstractElements.filter(ast_1.isModuleImport).flatMap(m => m.entities.filter(ast_1.isImportedEntity).map(e => this.descriptions.createDescription(e, `${m.ontology}.${e.$container.name}.${e.name}`, document)));
            root.abstractElements.filter(ast_1.isModuleImport).flatMap(m => m.entities.filter(ast_1.isImportedEntity).map(e => this.exportNode(e, default_global, document)));
            return default_global.concat(entities).concat(entities_imported);
        });
    }
}
exports.CustomScopeComputation = CustomScopeComputation;
//# sourceMappingURL=kashmir-scope.js.map