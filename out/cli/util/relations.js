"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processRelations = void 0;
const ast_1 = require("../../language-server/generated/ast");
function revert_card(card) {
    switch (card) {
        case 'OneToOne':
            return 'OneToOne';
        case 'OneToMany':
            return 'ManyToOne';
        case 'ManyToOne':
            return 'OneToMany';
        case 'ManyToMany':
            return 'ManyToMany';
    }
}
/**
 * Dado um módulo, lê todas as relações internas dele,
 * retornando um mapa que mapeia um Class para a lista
 * de {alvo, cardinalidade e ownership} de suas relações
 */
function processRelations(localEntities) {
    // Inicializa o mapa com listas vazias
    const map = new Map();
    for (const cls of localEntities) {
        map.set(cls, new Array());
    }
    const add_relation = (owner, non_owner, card_name) => {
        var _a, _b;
        (_a = map.get(owner)) === null || _a === void 0 ? void 0 : _a.push({
            tgt: non_owner,
            card: card_name,
            owner: true
        });
        (_b = map.get(non_owner)) === null || _b === void 0 ? void 0 : _b.push({
            tgt: owner,
            card: revert_card(card_name),
            owner: false
        });
    };
    for (const entity of localEntities) {
        for (const relationship of entity.relations) {
            if ((0, ast_1.isLocalEntity)(relationship.type.ref)) {
                if (relationship.$type === "OneToMany") {
                    add_relation(relationship.type.ref, entity, "ManyToOne");
                }
                else {
                    add_relation(entity, relationship.type.ref, relationship.$type);
                }
            }
        }
    }
    return map;
}
exports.processRelations = processRelations;
//# sourceMappingURL=relations.js.map