"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateMicronaut = void 0;
const path_1 = __importDefault(require("path"));
const main_generator_1 = require("./MicronautGenerator/main-generator");
function generateMicronaut(model, filePath, destination) {
    const final_destination = extractDestination(filePath, destination);
    (0, main_generator_1.generateMainMicronaut)(model, final_destination);
    return final_destination;
}
exports.generateMicronaut = generateMicronaut;
function extractDestination(filePath, destination) {
    const path_ext = new RegExp(path_1.default.extname(filePath) + '$', 'g');
    filePath = filePath.replace(path_ext, '');
    return destination !== null && destination !== void 0 ? destination : path_1.default.join(path_1.default.dirname(filePath), "generated");
}
//# sourceMappingURL=micronautGenerator.js.map