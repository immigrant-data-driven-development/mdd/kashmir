"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateJavaScript = void 0;
const path_1 = __importDefault(require("path"));
const main_generator_1 = require("./MicronautGenerator/main-generator");
const main_generator_2 = require("./SpringDataGenerator/main-generator");
const main_generator_3 = require("./DocumentationGenerator/main-generator");
function generateJavaScript(model, filePath, destination) {
    var _a, _b;
    const final_destination = extractDestination(filePath, destination);
    if (model.configuration) {
        console.log((_a = model.configuration) === null || _a === void 0 ? void 0 : _a.framework);
        if (((_b = model.configuration) === null || _b === void 0 ? void 0 : _b.framework) === 'micronaut') {
            (0, main_generator_1.generateMainMicronaut)(model, final_destination);
        }
        else {
            (0, main_generator_2.generateMainSpringData)(model, final_destination);
        }
    }
    (0, main_generator_3.generateDocumentation)(model, final_destination);
    return final_destination;
}
exports.generateJavaScript = generateJavaScript;
function extractDestination(filePath, destination) {
    const path_ext = new RegExp(path_1.default.extname(filePath) + '$', 'g');
    filePath = filePath.replace(path_ext, '');
    return destination !== null && destination !== void 0 ? destination : path_1.default.join(path_1.default.dirname(filePath), "generated");
}
//# sourceMappingURL=generator.js.map