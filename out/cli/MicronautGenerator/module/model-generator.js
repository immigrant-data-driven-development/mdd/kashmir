"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateModel = void 0;
const langium_1 = require("langium");
const ast_1 = require("../../../language-server/generated/ast");
const generator_utils_1 = require("../../util/generator-utils");
function generateModel(cls, is_supertype, relations, package_name) {
    var _a;
    const supertype = (_a = cls.superType) === null || _a === void 0 ? void 0 : _a.ref;
    const external_relations = relations.filter(relation => relation.tgt.$container != cls.$container);
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.models;

    import lombok.Getter;
    import lombok.Setter;
    import lombok.Builder;
    import lombok.NoArgsConstructor;
    import lombok.AllArgsConstructor;
    import lombok.experimental.SuperBuilder;

    import javax.persistence.Id;
    import javax.persistence.Inheritance;
    import javax.persistence.InheritanceType;
    import javax.persistence.Table;
    import javax.persistence.Entity;
    import javax.persistence.OneToOne;
    import javax.persistence.OneToMany;
    import javax.persistence.ManyToOne;
    import javax.persistence.ManyToMany;
    import javax.persistence.JoinColumn;
    import javax.persistence.JoinTable;
    import javax.persistence.CascadeType;
    import javax.persistence.GeneratedValue;

    import org.hibernate.annotations.GenericGenerator;

    import java.io.Serializable;
    import java.time.LocalDateTime;
    import java.util.Set;
    import java.util.HashSet;
    import java.util.Objects;
    import java.util.UUID;

    import java.util.Date;

    ${external_relations.map(relation => `import ${package_name.replace(cls.$container.name.toLowerCase(), relation.tgt.$container.name.toLowerCase())}.models.${relation.tgt.name};`)}

    @Getter
    @Setter
    @Entity
    @SuperBuilder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "${cls.name.toLowerCase()}")
    ${is_supertype ? '@Inheritance(strategy = InheritanceType.JOINED)' : undefined}
    public class ${cls.name} ${supertype ? `extends ${supertype.name}` : ''} implements Serializable {
        @Id
        @GeneratedValue(generator = "uuid")
        @GenericGenerator(name = "uuid", strategy = "uuid2")
        UUID id;

        ${cls.attributes.map(a => { var _a; return `${(0, generator_utils_1.capitalizeString)((_a = a.type) !== null && _a !== void 0 ? _a : 'NOTYPE')} ${a.name};`; }).join('\n')}
        ${generateRelations(cls, relations)}
        ${generateEnum(cls)}

        @Builder.Default
        LocalDateTime createdAt = LocalDateTime.now();

        @Override
        public boolean equals(Object o) {
              if (this == o) return true;
              if (o == null || this.getClass() != o.getClass()) return false;

            ${cls.name} elem = (${cls.name}) o;
            return getId().equals(elem.getId());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getId());
        }

        @Override
        public String toString() {
            return "${cls.name} {" +
                "id="+this.id+
                ${cls.attributes.map(a => `", ${a.name}='"+this.${a.name}+"'"+`).join('\n')}
                ${(0, ast_1.isLocalEntity)(supertype) ? supertype === null || supertype === void 0 ? void 0 : supertype.attributes.map(a => `", ${a.name}='"+this.${a.name}+"'"+`).join('\n') : ""} 
                ${cls.enumentityatributes.map(a => `", ${a.name.toLowerCase()}='"+this.${a.name.toLowerCase()}+"'"+`).join('\n')}
            '}';
        }  
    }
  `;
}
exports.generateModel = generateModel;
function generateRelations(cls, relations) {
    const node = new langium_1.CompositeGeneratorNode();
    for (const rel of relations) {
        node.append(generateRelation(cls, rel));
        node.appendNewLine();
    }
    return node;
}
function generateRelation(cls, { tgt, card, owner }) {
    switch (card) {
        case "OneToOne":
            if (owner) {
                return (0, langium_1.expandToStringWithNL) `
        @OneToOne
        @JoinColumn(name = "${tgt.name.toLowerCase()}_id", referencedColumnName = "id")
        private ${tgt.name} ${tgt.name.toLowerCase()};
      `;
            }
            else {
                return (0, langium_1.expandToStringWithNL) `
        @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "${cls.name.toLowerCase()}")
        @Builder.Default
        private ${tgt.name} ${tgt.name.toLowerCase()} = null;
      `;
            }
        case "OneToMany":
            if (owner) {
                return '';
            }
            else {
                return (0, langium_1.expandToStringWithNL) `
        @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "${cls.name.toLowerCase()}")
        @Builder.Default
        Set<${tgt.name}> ${tgt.name.toLowerCase()}s = new HashSet<>();
      `;
            }
        case "ManyToOne":
            if (owner) {
                return (0, langium_1.expandToStringWithNL) `
        @ManyToOne
        @JoinColumn(name = "${tgt.name.toLowerCase()}_id")
        private ${tgt.name} ${tgt.name.toLowerCase()};
      `;
            }
            else {
                return '';
            }
        case "ManyToMany":
            if (owner) {
                return (0, langium_1.expandToStringWithNL) `
        @ManyToMany
        @JoinTable(
            name = "${cls.name.toLowerCase()}_${tgt.name.toLowerCase()}",
            joinColumns = @JoinColumn(name = "${cls.name.toLowerCase()}_id"),
            inverseJoinColumns = @JoinColumn(name = "${tgt.name.toLowerCase()}_id")
        )
        @Builder.Default
        private Set<${tgt.name}> ${tgt.name.toLowerCase()}s = new HashSet<>();
      `;
            }
            else {
                return (0, langium_1.expandToStringWithNL) `
        @ManyToMany(mappedBy = "${cls.name.toLowerCase()}s")
        @Builder.Default
        private Set<${tgt.name}> ${tgt.name.toLowerCase()}s = new HashSet<>();
      `;
            }
    }
}
function createEnum(enumEntityAtribute) {
    var _a, _b, _c;
    return (0, langium_1.expandToString) `
  @Builder.Default
  private ${(_a = enumEntityAtribute.type.ref) === null || _a === void 0 ? void 0 : _a.name} ${enumEntityAtribute.name.toLowerCase()} = ${(_b = enumEntityAtribute.type.ref) === null || _b === void 0 ? void 0 : _b.name}.${(_c = enumEntityAtribute.type.ref) === null || _c === void 0 ? void 0 : _c.attributes[0].name.toUpperCase()};
  `;
}
function generateEnum(cls) {
    return (0, langium_1.expandToStringWithNL) `
  ${cls.enumentityatributes.map(enumEntityAtribute => createEnum(enumEntityAtribute)).join("\n")}
  `;
}
//# sourceMappingURL=model-generator.js.map