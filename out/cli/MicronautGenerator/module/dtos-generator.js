"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getOutputRelations = exports.getInputRelations = exports.generateOutputDTO = exports.generateInputDTO = void 0;
const langium_1 = require("langium");
const generator_utils_1 = require("../../util/generator-utils");
function processAttributes(attrs) {
    return attrs.map(a => { var _a; return `${(0, generator_utils_1.capitalizeString)((_a = a.type) !== null && _a !== void 0 ? _a : 'NOTYPE')} ${a.name}`; });
}
function processRelations(relations) {
    return relations.map(r => {
        if (r.card === "ManyToMany" || r.card === "OneToMany") {
            return `List<UUID> ${r.tgt.name.toLowerCase()}_ids`;
        }
        else {
            return `UUID ${r.tgt.name.toLowerCase()}_id`;
        }
    });
}
function generateInputDTO(cls, attributes, relations, package_name) {
    const attrs_str = processAttributes(attributes);
    const rels_str = processRelations(getInputRelations(relations));
    const enum_str = cls.enumentityatributes.map(enux => { var _a; return `${(_a = enux.type.ref) === null || _a === void 0 ? void 0 : _a.name} ${enux.name.toLowerCase()}`; });
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.dtos;

    import java.util.List;
    import java.util.UUID;
    import java.util.Date;
    // import javax.validation.constraints.NotBlank;
    import io.micronaut.core.annotation.Introspected;
    ${cls.enumentityatributes.map(enux => { var _a; return `import ${package_name}.models.${(_a = enux.type.ref) === null || _a === void 0 ? void 0 : _a.name};`; }).join('\n')}
    
    @Introspected
    public record ${cls.name}InputDto(
        ${attrs_str.concat(rels_str).concat(enum_str).map(langium_1.toString).join(',\n')}
        
    ) {}
  `;
}
exports.generateInputDTO = generateInputDTO;
function generateOutputDTO(cls, attributes, relations, package_name) {
    const attrs_str = processAttributes(attributes);
    const rels_str = processRelations(getOutputRelations(relations));
    const enum_str = cls.enumentityatributes.map(enux => { var _a; return `${(_a = enux.type.ref) === null || _a === void 0 ? void 0 : _a.name} ${enux.name.toLowerCase()}`; });
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.dtos;

    import java.util.List;
    import java.util.UUID;
    import java.time.LocalDateTime;
    import java.util.Date;
    // import javax.validation.constraints.NotNull;
    import javax.validation.constraints.NotBlank;
    // import javax.validation.constraints.PastOrPresent;
    ${cls.enumentityatributes.map(enux => { var _a; return `import ${package_name}.models.${(_a = enux.type.ref) === null || _a === void 0 ? void 0 : _a.name};`; }).join('\n')}
    
    public record ${cls.name}OutputDto(
        @NotBlank UUID id,
        ${attrs_str.concat(rels_str).concat(enum_str).map(str => (0, langium_1.toString)(str) + ',').join('\n')}
        LocalDateTime createdAt
    ) {}
  `;
}
exports.generateOutputDTO = generateOutputDTO;
/**
 * Dada uma lista de RelationInfo, retorna a lista de quais dessas relações são passadas no InputDTO.
 */
function getInputRelations(relations) {
    return relations.filter(r => r.owner);
}
exports.getInputRelations = getInputRelations;
/**
 * Dada uma lista de RelationInfo, retorna a lista de quais dessas relações são passadas no OutputDTO.
 *
 * No momento, isso se resume a "qualquer relação exceto OneToOne que você não é dono", pois pode ser um null pointer
 */
function getOutputRelations(relations) {
    return relations.filter(r => r.owner || !(r.card === "OneToOne"));
}
exports.getOutputRelations = getOutputRelations;
//# sourceMappingURL=dtos-generator.js.map