"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateConfigs = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const langium_1 = require("langium");
const generator_utils_1 = require("../util/generator-utils");
function generateConfigs(model, target_folder) {
    if (model.configuration) {
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'GUIDE.md'), (0, langium_1.toString)(generateGuide(model.configuration)));
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'docker-compose.yml'), (0, langium_1.toString)(generateCompose(model.configuration)));
        const RESOURCE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/resources");
        fs_1.default.writeFileSync(path_1.default.join(RESOURCE_PATH, 'application.yml'), (0, langium_1.toString)(generateApplication(model)));
    }
}
exports.generateConfigs = generateConfigs;
// TODO no comando micronaut, conferir se o nome `base.demo` tá certo ou não
function generateGuide(configuration) {
    return (0, langium_1.expandToStringWithNL) `
    # README

    ## Setup

    * Instale o Micronaut (3.8.1+) na sua máquina
    * Rode o seguinte comando, para gerar o projeto Micronaut:
      \`mn create-app --build=gradle --jdk=17 --lang=java --test=junit --features=postgres,openapi,data-jpa,lombok,assertj,testcontainers ${configuration.package_path}.${configuration.software_name}\`
    * Passe os todos os arquivos gerados pelo gerador para dentro da pasta gerada pelo Micronaut (Sobreescrevendo os que forem necessários)
    * Adicione a seguinte linha ao arquivo \`gradle.properties\`:
      * \`org.gradle.jvmargs=-Dmicronaut.openapi.views.spec=redoc.enabled=true,rapidoc.enabled=true,swagger-ui.enabled=true,swagger-ui.theme=flattop\`

    ## Rodando a aplicação

    * Vá para a pasta gerada pelo Micronaut (provavelmente se chama 'demo')
    * Suba o docker do banco de dados com o comando \`docker compose up -d postgres\`
    * Rode a aplicação com o comando \`./gradlew run\`
    * A interface da API estará rodando em \`localhost:8080/swagger/views/swagger-ui/\` via SwaggerUI e em \`localhost:8080/swagger/views/rapidoc/\` via RapiDoc

    ## Bugs Conhecidos

    * Os arquivos InputDTO PODEM ter uma vírgula a mais do que deveriam - uma vírgula depois do último argumento. Elas precisam ser removidas manualmente
  `;
}
function generateCompose(configuration) {
    var _a, _b;
    return (0, langium_1.expandToStringWithNL) `
    version: '3.7'

    services:
      postgres:
        image: postgres
        ports:
          - "5432:5432"
        restart: always
        environment:
          POSTGRES_PASSWORD: postgres
          POSTGRES_DB: ${(_b = (_a = configuration.database_name) !== null && _a !== void 0 ? _a : configuration.software_name) !== null && _b !== void 0 ? _b : 'KashmirDB'}
          POSTGRES_USER: postgres
        volumes:
          - ./data:/var/lib/postgresql
          - ./pg-initdb.d:/docker-entrypoint-initdb.d
  `;
}
function generateApplication(model) {
    var _a, _b, _c, _d, _e, _f;
    return (0, langium_1.expandToStringWithNL) `
    micronaut:
      application:
        name: ${(_b = (_a = model.configuration) === null || _a === void 0 ? void 0 : _a.software_name) !== null && _b !== void 0 ? _b : path_1.default.basename((_d = (_c = model.$document) === null || _c === void 0 ? void 0 : _c.uri.path) !== null && _d !== void 0 ? _d : '')}
      router:
        static-resources:
          swagger:
            paths: classpath:META-INF/swagger
            mapping: /swagger/**
          swagger-ui:
            paths: classpath:META-INF/swagger/views/swagger-ui
            mapping: /swagger-ui/**
          rapidoc:
            paths: classpath:META-INF/swagger/views/rapidoc
            mapping: /rapidoc/**
          redoc:
            paths: classpath:META-INF/swagger/views/redoc-ui
            mapping: /redoc/**

    datasources:
      default:
        url: jdbc:postgresql://localhost:5432/${(_f = (_e = model.configuration) === null || _e === void 0 ? void 0 : _e.database_name) !== null && _f !== void 0 ? _f : '#Database Name is not Configured'}
        username: postgres
        password: postgres
        driver-class-name: org.postgresql.Driver
        schema-generate: CREATE_DROP
        db-type: postgres
        dialect: POSTGRES
    jpa.default.properties.hibernate.hbm2ddl.auto: update
    jpa:
    default:
      properties:
        hibernate:
          hbm2ddl:
            auto: update
    netty:
      default:
        allocator:
          max-order: 3
  `;
}
//# sourceMappingURL=config-generator.js.map