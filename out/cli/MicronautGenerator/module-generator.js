"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateModules = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const ast_1 = require("../../language-server/generated/ast");
const generator_utils_1 = require("../util/generator-utils");
const relations_1 = require("../util/relations");
const langium_1 = require("langium");
const model_generator_1 = require("./module/model-generator");
const dtos_generator_1 = require("./module/dtos-generator");
const exception_generator_1 = require("./module/exception-generator");
const controller_generator_1 = require("./module/controller-generator");
const enum_generator_1 = require("./module/enum-generator");
function generateModules(model, target_folder) {
    var _a, _b;
    const package_path = (_b = (_a = model.configuration) === null || _a === void 0 ? void 0 : _a.package_path) !== null && _b !== void 0 ? _b : 'base';
    const modules = model.abstractElements.filter(ast_1.isModule);
    const all_entities = modules.map(module => module.elements.filter(ast_1.isLocalEntity)).flat();
    const relation_maps = (0, relations_1.processRelations)(all_entities);
    for (const mod of modules) {
        const package_name = `${package_path}.${mod.name.toLowerCase()}`;
        const MODULE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", package_name.replaceAll(".", "/"));
        const EXCEPTIONS_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'exceptions');
        const APPLICATIONS_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'applications');
        const REPOSITORIES_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'repositories');
        const CONTROLLERS_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'controllers');
        const MODELS_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'models');
        const DTOS_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'dtos');
        fs_1.default.writeFileSync(path_1.default.join(EXCEPTIONS_PATH, 'NotFoundException.java'), (0, langium_1.toString)((0, exception_generator_1.generateNotFoundException)(package_name)));
        fs_1.default.writeFileSync(path_1.default.join(EXCEPTIONS_PATH, 'NotFoundHandler.java'), (0, langium_1.toString)((0, exception_generator_1.generateNotFoundHandler)(package_name)));
        const supertype_classes = processSupertypes(mod);
        const mod_classes = mod.elements.filter(ast_1.isLocalEntity);
        for (const cls of mod_classes) {
            const class_name = cls.name;
            const { attributes, relations } = getAttrsAndRelations(cls, relation_maps);
            fs_1.default.writeFileSync(path_1.default.join(MODELS_PATH, `${class_name}.java`), (0, langium_1.toString)((0, model_generator_1.generateModel)(cls, supertype_classes.has(cls), relations, package_name)));
            fs_1.default.writeFileSync(path_1.default.join(APPLICATIONS_PATH, `${class_name}Apl.java`), (0, langium_1.toString)(generateApplication(cls, package_name)));
            fs_1.default.writeFileSync(path_1.default.join(DTOS_PATH, `${class_name}InputDto.java`), (0, langium_1.toString)((0, dtos_generator_1.generateInputDTO)(cls, attributes, relations, package_name)));
            fs_1.default.writeFileSync(path_1.default.join(DTOS_PATH, `${class_name}OutputDto.java`), (0, langium_1.toString)((0, dtos_generator_1.generateOutputDTO)(cls, attributes, relations, package_name)));
            fs_1.default.writeFileSync(path_1.default.join(CONTROLLERS_PATH, `${class_name}Controller.java`), (0, langium_1.toString)((0, controller_generator_1.generateController)(cls, attributes, relations, package_name)));
            fs_1.default.writeFileSync(path_1.default.join(REPOSITORIES_PATH, `${class_name}Repository.java`), (0, langium_1.toString)(generateClassRepository(cls, package_name)));
            fs_1.default.writeFileSync(path_1.default.join(EXCEPTIONS_PATH, `${class_name}NotFoundException.java`), (0, langium_1.toString)((0, exception_generator_1.generateClassNotFoundException)(cls, package_name)));
        }
        for (const enumx of mod.elements.filter(ast_1.isEnumX)) {
            fs_1.default.writeFileSync(path_1.default.join(MODELS_PATH, `${enumx.name}.java`), (0, enum_generator_1.generateEnum)(enumx, package_name));
        }
    }
}
exports.generateModules = generateModules;
/**
 * Dado um módulo, retorna todos as classes dele que são usadas como Superclasses
 */
function processSupertypes(mod) {
    var _a, _b;
    const set = new Set();
    for (const cls of mod.elements.filter(ast_1.isLocalEntity)) {
        if (((_a = cls.superType) === null || _a === void 0 ? void 0 : _a.ref) != null && (0, ast_1.isLocalEntity)(cls)) {
            set.add((_b = cls.superType) === null || _b === void 0 ? void 0 : _b.ref);
        }
    }
    return set;
}
/**
 * Retorna todos os atributos e relações de uma Class, incluindo a de seus supertipos
 */
function getAttrsAndRelations(cls, relation_map) {
    var _a, _b, _c, _d, _e;
    // Se tem superclasse, puxa os atributos e relações da superclasse
    if (((_a = cls.superType) === null || _a === void 0 ? void 0 : _a.ref) != null && (0, ast_1.isLocalEntity)((_b = cls.superType) === null || _b === void 0 ? void 0 : _b.ref)) {
        const parent = (_c = cls.superType) === null || _c === void 0 ? void 0 : _c.ref;
        const { attributes, relations } = getAttrsAndRelations(parent, relation_map);
        return {
            attributes: attributes.concat(cls.attributes),
            relations: relations.concat((_d = relation_map.get(cls)) !== null && _d !== void 0 ? _d : [])
        };
    }
    else {
        return {
            attributes: cls.attributes,
            relations: (_e = relation_map.get(cls)) !== null && _e !== void 0 ? _e : []
        };
    }
}
function generateClassRepository(cls, package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.repositories;

    import ${package_name}.models.${cls.name};

    import java.util.UUID;
    import io.micronaut.data.annotation.Repository;
    import io.micronaut.data.jpa.repository.JpaRepository;

    @Repository
    public interface ${cls.name}Repository extends JpaRepository<${cls.name}, UUID>{

    }
  `;
}
function generateApplication(cls, package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.applications;

    import ${package_name}.models.${cls.name};
    import ${package_name}.repositories.${cls.name}Repository;

    import java.util.List;
    import java.util.Optional;
    import java.util.UUID;
    import jakarta.inject.Singleton;
    import lombok.RequiredArgsConstructor;

    @Singleton
    @RequiredArgsConstructor
    public class ${cls.name}Apl {
        private final ${cls.name}Repository repo;

        public ${cls.name} save(${cls.name} p) {
            return this.repo.save(p);
        }

        public List<${cls.name}> findAll() {
            return this.repo.findAll();
        }

        public Optional<${cls.name}> findById(UUID id) {
            return this.repo.findById(id);
        }

        public ${cls.name} update(${cls.name} p) {
            return this.repo.update(p);
        }

        public void delete(${cls.name} p) {
            this.repo.delete(p);
        }
    }
  `;
}
//# sourceMappingURL=module-generator.js.map