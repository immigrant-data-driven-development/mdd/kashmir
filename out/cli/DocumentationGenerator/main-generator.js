"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateDocumentation = void 0;
const fs_1 = __importDefault(require("fs"));
const langium_1 = require("langium");
const generator_utils_1 = require("../util/generator-utils");
const ast_1 = require("../../language-server/generated/ast");
const path_1 = __importDefault(require("path"));
const ident = generator_utils_1.base_ident;
function generateDocumentation(model, target_folder) {
    var _a;
    fs_1.default.mkdirSync(target_folder, { recursive: true });
    if (model.configuration) {
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'README.md'), createProjectReadme(model.configuration));
        fs_1.default.writeFileSync(path_1.default.join(target_folder, '.gitlab-ci.yml'), createGitlab());
    }
    const DOCS_PATH = (0, generator_utils_1.createPath)(target_folder, "docs");
    const modules = model.abstractElements.filter(ast_1.isModule);
    fs_1.default.writeFileSync(path_1.default.join(DOCS_PATH, "README.md"), generalREADME(modules));
    fs_1.default.writeFileSync(path_1.default.join(DOCS_PATH, "packagediagram.puml"), createPackageDiagram(modules, (_a = model.configuration) === null || _a === void 0 ? void 0 : _a.software_name));
    for (const m of modules) {
        const MODULE_PATH = (0, generator_utils_1.createPath)(DOCS_PATH, m.name.toLowerCase());
        fs_1.default.writeFileSync(path_1.default.join(MODULE_PATH, "/README.md"), moduleREADME(m));
        fs_1.default.writeFileSync(path_1.default.join(MODULE_PATH, "/classdiagram.puml"), createClassDiagram(m));
    }
}
exports.generateDocumentation = generateDocumentation;
function createGitlab() {
    return (0, langium_1.expandToStringWithNL) `
    variables:
    CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest

    stages:
    - build-entity
    - build-webservice
    - release-webservice

    maven-build:
    stage: build-entity
    image: maven:latest
    script: 
        - cd entity
        - mvn deploy -s settings.xml -DskipTests

    build-webservice:
    stage: build-webservice
    image: docker:20.10.16
    services:
        - docker:20.10.16-dind
    before_script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    script:
        - cd webservice
        - docker build --pull -t $CONTAINER_TEST_IMAGE .
        - docker push $CONTAINER_TEST_IMAGE

    release-master-webservice:
    stage: release-webservice
    image: docker:20.10.16
    services:
        - docker:20.10.16-dind
    before_script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    script:
        - cd webservice
        - docker pull $CONTAINER_TEST_IMAGE
        - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
        - docker push $CONTAINER_RELEASE_IMAGE
    only:
        - main

    release-dev-webservice:
    stage: release-webservice
    script:
        - cd webservice
        - docker pull $CONTAINER_TEST_IMAGE
        - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_TEST_IMAGE
        - docker push $CONTAINER_TEST_IMAGE
    only:
        - dev

    `;
}
function createPackageDiagram(modules, name) {
    return (0, langium_1.expandToStringWithNL) `
    @startuml ${name !== null && name !== void 0 ? name : ''}
    ${modules.flatMap(m => `namespace ${m.name}{}`).join('\n')}
    @enduml`;
}
function createClassDiagram(m) {
    const enums = m.elements.filter(ast_1.isEnumX);
    const entities = m.elements.filter(ast_1.isLocalEntity);
    return (0, langium_1.expandToStringWithNL) `
    @startuml ${m.name}
        ${enums.flatMap(e => [`enum ${e.name} { \n ${e.attributes.map(a => `${ident}${a.name}`).join(`\n`)}\n}`]).join(`\n`)}
        ${entities.map(e => entityClassDiagram(e, m)).join(`\n`)}
    @enduml`;
}
function entityClassDiagram(e, m) {
    var _a;
    const lines = [
        `class ${e.name} ${e.superType ? (0, ast_1.isImportedEntity)(e.superType.ref) ? `<< ${e.superType.ref.name}>>` : `` : ``}{`,
        ...e.attributes.map(a => `${a.type}: ${a.name}`),
        ``,
        ...e.relations.filter(r => !(0, ast_1.isManyToMany)(r)).map(r => { var _a; return `${(_a = r.type.ref) === null || _a === void 0 ? void 0 : _a.name}: ${r.name.toLowerCase()}`; }),
        `}`,
        ((_a = e.superType) === null || _a === void 0 ? void 0 : _a.ref) ? `\n${e.superType.ref.name} <|-- ${e.name}\n` : '',
        e.enumentityatributes.map(a => { var _a; return `${e.name} "1" -- "1" ${(_a = a.type.ref) === null || _a === void 0 ? void 0 : _a.name} : ${a.name.toLowerCase()}>`; }),
        ...e.relations.filter(r => !(0, ast_1.isManyToOne)(r)).map(r => relationDiagram(r, e, m)),
        ``
    ];
    return lines.join('\n');
}
function relationDiagram(r, e, m) {
    var _a, _b, _c;
    // Cardinalidades
    const tgt_card = (0, ast_1.isOneToOne)(r) ? "1" : "0..*";
    const src_card = (0, ast_1.isManyToMany)(r) ? "0..*" : "1";
    // Módulo de origem da entidade destino
    const origin_module = ((_a = r.type.ref) === null || _a === void 0 ? void 0 : _a.$container.name.toLowerCase()) !== m.name.toLowerCase() ?
        `${(_b = r.type.ref) === null || _b === void 0 ? void 0 : _b.$container.name}.` :
        "";
    return `${e.name} "${src_card}" -- "${tgt_card}" ${origin_module}${(_c = r.type.ref) === null || _c === void 0 ? void 0 : _c.name} : ${r.name.toLowerCase()} >`;
}
function moduleREADME(m) {
    var _a;
    const lines = [
        `# 📕Documentation: ${m.name}`,
        ``,
        `${(_a = m.description) !== null && _a !== void 0 ? _a : ''}`,
        ``,
        `## 🌀 Package's Data Model`,
        ``,
        `![Domain Diagram](classdiagram.png)`,
        ``,
        `### ⚡Entities`,
        ``,
        ...m.elements.filter(ast_1.isLocalEntity).map(e => { var _a; return `* **${e.name}** : ${(_a = e.description) !== null && _a !== void 0 ? _a : '-'}`; }),
        ``
    ];
    return lines.join('\n');
}
function generalREADME(modules) {
    return (0, langium_1.expandToStringWithNL) `
    # 📕Documentation

    ## 🌀 Project's Package Model
    ![Domain Diagram](packagediagram.png)

    ### 📲 Modules
    ${modules.map(m => { var _a; return `* **[${m.name}](./${m.name.toLocaleLowerCase()}/)** :${(_a = m.description) !== null && _a !== void 0 ? _a : '-'}`; }).join("\n")}

    `;
}
function stackREADME(stack) {
    if (stack == "springboot") {
        return (0, langium_1.expandToStringWithNL) `
        1. Spring Boot 3.0
        2. Spring Data Rest
        3. Spring GraphQL
        `;
    }
    return (0, langium_1.expandToStringWithNL) `
    1. Micronaut 3.8.2    
    `;
}
function createProjectReadme(configuration) {
    return (0, langium_1.expandToStringWithNL) `
    # ${configuration.software_name}
    ## 🚀 Goal
    ${configuration.about}

    ## 📕 Domain Documentation
    
    Domain documentation can be found [here](./docs/README.md)

    ## ⚙️ Requirements

    1. Postgresql
    2. Java 17
    3. Maven

    ## ⚙️ Stack 
    ${stackREADME(configuration.framework)}

    ## 🔧 Install

    1) Create a database with name ${configuration.software_name} with **CREATE DATABASE ${configuration.software_name}**.
    2) Run the command to start the webservice and create table of database:

    \`\`\`bash
    mvn Spring-boot:run 
    \`\`\`

    ## Debezium

    Go to folder named *register* and performs following command to register in debezium:

    \`\`\`bash
    curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-sro.json
    \`\`\`

    To delete, uses:

    \`\`\`bash
    curl -i -X DELETE localhost:8083/connectors/sro-connector/
    \`\`\`
        
    
    ## 🔧 Usage

    * Access [http://localhost:${configuration.service_port}](http://localhost:${configuration.service_port}) to see Swagger 
    * Acess [http://localhost:${configuration.service_port}/grapiql](http://localhost:${configuration.service_port}/grapiql) to see Graphql.

    ## ✒️ Team
    
    * **[${configuration.author}](${configuration.author_email})**
    
    ## 📕 Literature

    `;
}
//# sourceMappingURL=main-generator.js.map