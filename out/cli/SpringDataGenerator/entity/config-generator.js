"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateConfigs = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const ast_1 = require("../../../language-server/generated/ast");
const langium_1 = require("langium");
const generator_utils_1 = require("../../util/generator-utils");
function generateConfigs(model, target_folder) {
    if (model.configuration) {
        const RESOURCE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/resources");
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'settings.xml'), (0, langium_1.toString)(generateSettings()));
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'pom.xml'), (0, langium_1.toString)(generatePOMXML(model)));
        fs_1.default.writeFileSync(path_1.default.join(RESOURCE_PATH, 'logback.xml'), (0, langium_1.toString)(generatelogback()));
        fs_1.default.writeFileSync(path_1.default.join(RESOURCE_PATH, 'application.properties'), (0, langium_1.toString)(applicationProperties(model.configuration)));
    }
}
exports.generateConfigs = generateConfigs;
function generateSettings() {
    return (0, langium_1.expandToStringWithNL) `
    <settings>
    <servers>
      <server>
        <id>gitlab-maven</id>
        <configuration>
          <httpHeaders>
            <property>
              <name>Private-Token</name>
              <value>\${CI_JOB_TOKEN}</value>
            </property>
          </httpHeaders>
        </configuration>
      </server>   
    </servers>
  </settings>`;
}
function applicationProperties(configuration) {
    return (0, langium_1.expandToStringWithNL) `
  spring.datasource.initialization-mode=always
  spring.datasource.url =  jdbc:postgresql://localhost:5432/${configuration.database_name}
  spring.datasource.username = postgres
  spring.datasource.password = postgres
  spring.datasource.platform= postgres
  #spring.jpa.hibernate.ddl-auto = update
  spring.jpa.hibernate.ddl-auto = create-drop
  
  spring.jpa.properties.javax.persistence.schema-generation.create-source=metadata
  spring.jpa.properties.javax.persistence.schema-generation.scripts.action=create-drop
  spring.jpa.properties.javax.persistence.schema-generation.scripts.drop-target=sql/${configuration.database_name.toLowerCase()}.sql
  spring.jpa.properties.javax.persistence.schema-generation.scripts.create-target=sql/${configuration.database_name.toLowerCase()}.sql

  `;
}
function generatelogback() {
    return (0, langium_1.expandToStringWithNL) `
  <?xml version="1.0" encoding="UTF-8"?>
  <configuration>
      <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
          <encoder>
              <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n
              </pattern>
          </encoder>
      </appender>

      <root level="INFO">
          <appender-ref ref="STDOUT" />
      </root>
  </configuration>
  `;
}
function generatePOMXML(application) {
    var _a, _b, _c, _d;
    return (0, langium_1.expandToStringWithNL) `
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.1.0</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>${(_a = application.configuration) === null || _a === void 0 ? void 0 : _a.package_path}.entity</groupId>
	<artifactId>${(_b = application.configuration) === null || _b === void 0 ? void 0 : _b.software_name}</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>${(_c = application.configuration) === null || _c === void 0 ? void 0 : _c.software_name}</name>
	<description>${(_d = application.configuration) === null || _d === void 0 ? void 0 : _d.about}</description>
	<properties>
		<java.version>17</java.version>    
	</properties>

  <repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/#ADDGROUPID/-/packages/maven</url>
  </repository>

</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/#ADDPROJECTID/packages/maven</url>
  </repository>
  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/#ADDPROJECTID/packages/maven</url>
  </snapshotRepository>
</distributionManagement>



	<dependencies>
		
    ${application.abstractElements.filter(ast_1.isModuleImport).map(moduleImport => generateOntologyDependency(moduleImport)).join("\n")}
  

    <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>

    <dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>
    <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <scope>runtime</scope>
    </dependency>

    <dependency>
        <groupId>org.springframework.data</groupId>
        <artifactId>spring-data-commons</artifactId>
    </dependency>

	</dependencies>


</project>
  `;
}
function generateOntologyDependency(moduleImported) {
    return (0, langium_1.expandToStringWithNL) `
  <dependency>
  <groupId>br.nemo.immigrant.ontology.entity</groupId>
  <artifactId>${moduleImported.ontology.toLowerCase()}</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
  `;
}
//# sourceMappingURL=config-generator.js.map