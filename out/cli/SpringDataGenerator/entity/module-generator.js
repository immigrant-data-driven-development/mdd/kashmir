"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateModules = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const ast_1 = require("../../../language-server/generated/ast");
const generator_utils_1 = require("../../util/generator-utils");
const relations_1 = require("../../util/relations");
const langium_1 = require("langium");
const model_generator_1 = require("./model-generator");
const enum_generator_1 = require("./enum-generator");
function generateModules(model, target_folder) {
    var _a, _b, _c;
    const package_path = (_b = (_a = model.configuration) === null || _a === void 0 ? void 0 : _a.package_path) !== null && _b !== void 0 ? _b : 'base';
    const modules = model.abstractElements.filter(ast_1.isModule);
    const all_entities = modules.map(module => module.elements.filter(ast_1.isLocalEntity)).flat();
    const relation_maps = (0, relations_1.processRelations)(all_entities);
    const imported_entities = processImportedEntities(model);
    for (const mod of modules) {
        const package_name = `${package_path}.entity.${(_c = model.configuration) === null || _c === void 0 ? void 0 : _c.software_name}.${mod.name.toLowerCase()}`;
        const MODULE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", package_name.replaceAll(".", "/"));
        const REPOSITORIES_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'repositories');
        const MODELS_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'models');
        const supertype_classes = processSupertypes(mod);
        const mod_classes = mod.elements.filter(ast_1.isLocalEntity);
        for (const cls of mod_classes) {
            const class_name = cls.name;
            const { attributes, relations } = getAttrsAndRelations(cls, relation_maps);
            attributes;
            fs_1.default.writeFileSync(path_1.default.join(MODELS_PATH, `${class_name}.java`), (0, langium_1.toString)((0, model_generator_1.generateModel)(cls, supertype_classes.has(cls), relations, package_name, imported_entities)));
            if (!cls.is_abstract) {
                fs_1.default.writeFileSync(path_1.default.join(REPOSITORIES_PATH, `${class_name}Repository.java`), (0, langium_1.toString)(generateClassRepository(cls, package_name, imported_entities)));
            }
        }
        for (const enumx of mod.elements.filter(ast_1.isEnumX)) {
            fs_1.default.writeFileSync(path_1.default.join(MODELS_PATH, `${enumx.name}.java`), (0, enum_generator_1.generateEnum)(enumx, package_name));
        }
    }
}
exports.generateModules = generateModules;
function processImportedEntities(application) {
    const map = new Map();
    for (const moduleImport of application.abstractElements.filter(ast_1.isModuleImport)) {
        moduleImport.entities.map(importedEntity => map.set(importedEntity, moduleImport));
    }
    return map;
}
/**
 * Dado um módulo, retorna todos as classes dele que são usadas como Superclasses
 */
function processSupertypes(mod) {
    var _a, _b, _c;
    const set = new Set();
    for (const cls of mod.elements.filter(ast_1.isLocalEntity)) {
        if (((_a = cls.superType) === null || _a === void 0 ? void 0 : _a.ref) != null && (0, ast_1.isLocalEntity)((_b = cls.superType) === null || _b === void 0 ? void 0 : _b.ref)) {
            set.add((_c = cls.superType) === null || _c === void 0 ? void 0 : _c.ref);
        }
    }
    return set;
}
/**
 * Retorna todos os atributos e relações de uma Class, incluindo a de seus supertipos
 */
function getAttrsAndRelations(cls, relation_map) {
    var _a, _b, _c, _d, _e;
    // Se tem superclasse, puxa os atributos e relações da superclasse
    if (((_a = cls.superType) === null || _a === void 0 ? void 0 : _a.ref) != null && (0, ast_1.isLocalEntity)((_b = cls.superType) === null || _b === void 0 ? void 0 : _b.ref)) {
        const parent = (_c = cls.superType) === null || _c === void 0 ? void 0 : _c.ref;
        const { attributes, relations } = getAttrsAndRelations(parent, relation_map);
        return {
            attributes: attributes.concat(cls.attributes),
            relations: relations.concat((_d = relation_map.get(cls)) !== null && _d !== void 0 ? _d : [])
        };
    }
    else {
        return {
            attributes: cls.attributes,
            relations: (_e = relation_map.get(cls)) !== null && _e !== void 0 ? _e : []
        };
    }
}
function generateClassRepository(cls, package_name, importedEntities) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.repositories;

    import ${package_name}.models.${cls.name};
    import org.springframework.data.repository.PagingAndSortingRepository;
    import org.springframework.data.repository.ListCrudRepository;
    import java.util.Optional;
    import br.nemo.immigrant.ontology.entity.eo.teams.repositories.projections.IDProjection;

    public interface ${cls.name}Repository extends PagingAndSortingRepository<${cls.name}, Long>, ListCrudRepository<${cls.name}, Long> {

      Optional<IDProjection> findByExternalId(String externalId);

      Optional<IDProjection> findByInternalId(String internalId);

      Boolean existsByInternalId(String internalId);
    
    }
  `;
}
//# sourceMappingURL=module-generator.js.map