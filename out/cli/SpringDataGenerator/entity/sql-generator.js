"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateSchemaSQLHelper = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const ast_1 = require("../../../language-server/generated/ast");
const langium_1 = require("langium");
const generator_utils_1 = require("../../util/generator-utils");
function generateSchemaSQLHelper(model, target_folder) {
    if (model.configuration) {
        // criando a pasta que salva o SQL
        const SQL_PATH = (0, generator_utils_1.createPath)(target_folder, "sql");
        fs_1.default.writeFileSync(path_1.default.join(SQL_PATH, 'sql_unique_constrains.sql'), (0, langium_1.toString)(generateSQL(model)));
    }
}
exports.generateSchemaSQLHelper = generateSchemaSQLHelper;
function generateSQLCommand(entity) {
    var atributesUnique = [];
    for (const attribute of entity.attributes) {
        if ((attribute === null || attribute === void 0 ? void 0 : attribute.unique) && !entity.is_abstract) {
            atributesUnique.push(attribute);
        }
    }
    if (atributesUnique.length) {
        return (0, langium_1.expandToString) `
  ALTER TABLE public.${entity.name.toLowerCase()} ADD CONSTRAINT ${entity.name.toLowerCase()}_unique_constrain UNIQUE (${atributesUnique.map(a => `${a.name}`).join(`,`)});
  `;
    }
    return undefined;
}
function generateSQL(model) {
    return (0, langium_1.expandToStringWithNL) `
    ${model.abstractElements.filter(ast_1.isModule).map(module => module.elements.filter(ast_1.isLocalEntity).map(entity => generateSQLCommand(entity)).join('\n')).join('\n')}  
    `;
}
//# sourceMappingURL=sql-generator.js.map