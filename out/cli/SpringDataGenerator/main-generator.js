"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateMainSpringData = void 0;
const fs_1 = __importDefault(require("fs"));
const webservice_main_generator_1 = require("./webservice/webservice-main-generator");
const entity_main_generator_1 = require("./entity/entity-main-generator");
function generateMainSpringData(model, target_folder) {
    const target_folder_entity = target_folder + "/entity";
    const target_folder_webservice = target_folder + "/webservice";
    fs_1.default.mkdirSync(target_folder_entity, { recursive: true });
    fs_1.default.mkdirSync(target_folder_webservice, { recursive: true });
    (0, webservice_main_generator_1.generateWebserviceSpringData)(model, target_folder_webservice);
    (0, entity_main_generator_1.generateEntiySpringData)(model, target_folder_entity);
}
exports.generateMainSpringData = generateMainSpringData;
//# sourceMappingURL=main-generator.js.map