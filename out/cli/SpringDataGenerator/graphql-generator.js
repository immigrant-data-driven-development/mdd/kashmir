"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateGraphQL = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const ast_1 = require("../../language-server/generated/ast");
const langium_1 = require("langium");
const generator_utils_1 = require("../util/generator-utils");
function generateGraphQL(model, target_folder) {
    if (model.configuration) {
        const RESOURCE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/resources");
        const GRAPHQL_PATH = (0, generator_utils_1.createPath)(RESOURCE_PATH, "graphql");
        fs_1.default.writeFileSync(path_1.default.join(GRAPHQL_PATH, 'schema.graphqls'), (0, langium_1.toString)(generateSchemaGraphQL(model)));
    }
}
exports.generateGraphQL = generateGraphQL;
function generateRelationSchemaGraphQL(relation) {
    var _a, _b;
    switch (relation.$type) {
        case "OneToMany": return `[${(_a = relation.type.ref) === null || _a === void 0 ? void 0 : _a.name}]`;
        default: return `${(_b = relation.type.ref) === null || _b === void 0 ? void 0 : _b.name}`;
    }
    return "";
}
function generateTypeSchemaGraphQL(entity) {
    return (0, langium_1.expandToStringWithNL) `
    type ${entity.name}{
        id: ID!,
        ${entity.attributes.map(atribute => `${atribute.name}:String`).join("\n")}
        ${entity.relations.map(relation => `${relation.name}:${generateRelationSchemaGraphQL(relation)}`).join("\n")}
    }
    `;
}
function generateInputTypeSchemaGraphQL(entity) {
    return (0, langium_1.expandToStringWithNL) `
    input ${entity.name}Input{
      ${entity.attributes.map(atribute => `${atribute.name}:String!`).join("\n")}
      ${entity.relations.map(relation => `${relation.name}ID:String`).join("\n")}
    }
    `;
}
function generateSchemaGraphQL(application) {
    const modules = application.abstractElements.filter(ast_1.isModule);
    const all_entities = modules.map(module => module.elements.filter(ast_1.isLocalEntity)).flat();
    return (0, langium_1.expandToStringWithNL) `
    
    ${all_entities.map(entity => (0, langium_1.toString)(generateTypeSchemaGraphQL(entity))).join("\n")}
    
    ${all_entities.map(entity => (0, langium_1.toString)(generateInputTypeSchemaGraphQL(entity))).join("\n")}
  
    type Query{
      ${all_entities.map(entity => `findAll${entity.name}s:[${entity.name}]`).join("\n")}
      ${all_entities.map(entity => `findByID${entity.name} (ID: String):${entity.name}`).join("\n")}
    }
    
    type Mutation{
      ${all_entities.map(entity => `create${entity.name}(input: ${entity.name}Input):${entity.name}`).join("\n")}
      ${all_entities.map(entity => `delete${entity.name} (id: ID!, input: ${entity.name}Input):${entity.name}`).join("\n")}
      ${all_entities.map(entity => `update${entity.name} (id: ID!, input: ${entity.name}Input):${entity.name}`).join("\n")}
    }`;
}
//# sourceMappingURL=graphql-generator.js.map