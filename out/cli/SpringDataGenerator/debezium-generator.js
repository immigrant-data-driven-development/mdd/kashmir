"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateDebezium = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const ast_1 = require("../../language-server/generated/ast");
const langium_1 = require("langium");
const generator_utils_1 = require("../util/generator-utils");
function generateDebezium(model, target_folder) {
    if (model.configuration) {
        // criando a pasta que salva o SQL
        const SQL_PATH = (0, generator_utils_1.createPath)(target_folder, "sql");
        fs_1.default.writeFileSync(path_1.default.join(SQL_PATH, 'debezium.sql'), (0, langium_1.toString)(generateDebeziumSQL(model)));
        const REGISTER_PATH = (0, generator_utils_1.createPath)(target_folder, "register");
        fs_1.default.writeFileSync(path_1.default.join(REGISTER_PATH, model.configuration.software_name.toLowerCase() + '-register.json'), (0, langium_1.toString)(generateDebeziumRegister(model.configuration)));
    }
}
exports.generateDebezium = generateDebezium;
function generateDebeziumSQL(model) {
    return (0, langium_1.expandToStringWithNL) `
    ${model.abstractElements.filter(ast_1.isModule).map(module => module.elements.filter(ast_1.isLocalEntity).map(entity => !entity.is_abstract ? `ALTER TABLE public.${entity.name.toLowerCase()}  REPLICA IDENTITY FULL;` : undefined).join('\n')).join('\n')}  
    `;
}
function generateDebeziumRegister(configuration) {
    return (0, langium_1.expandToStringWithNL) `
    {
      "name": "${configuration.software_name.toLowerCase()}-connector",
      "config": {
          "connector.class": "io.debezium.connector.postgresql.PostgresConnector",
          "tasks.max": "1",
          "database.hostname": "db-pg",
          "database.port": "5432",
          "database.user": "postgres",
          "database.password": "postgres",
          "database.dbname" : "${configuration.software_name.toLowerCase()}",
          "topic.prefix": "ontology.${configuration.software_name.toLowerCase()}",
          "topic.partitions": 3,
          "schema.include.list": "public"
      }
  }
    
    `;
}
//# sourceMappingURL=debezium-generator.js.map