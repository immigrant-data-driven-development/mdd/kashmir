"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateGraphQL = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
//Relation
const ast_1 = require("../../../language-server/generated/ast");
const langium_1 = require("langium");
const generator_utils_1 = require("../../util/generator-utils");
function generateGraphQL(application, target_folder) {
    if (application.configuration) {
        const RESOURCE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/resources");
        const GRAPHQL_PATH = (0, generator_utils_1.createPath)(RESOURCE_PATH, "graphql");
        fs_1.default.writeFileSync(path_1.default.join(GRAPHQL_PATH, 'schema.graphqls'), (0, langium_1.toString)(generateSchemaGraphQL(application)));
    }
}
exports.generateGraphQL = generateGraphQL;
/*
function generateRelationSchemaGraphQL(relation: Relation): Generated{
    switch(relation.$type) {
      case "OneToMany": return `[${relation.type.ref?.name}]`
      default: return `${relation.type.ref?.name}`
    }
    return ""
  }
  */
function generateTypeSchemaGraphQL(entity) {
    var _a, _b, _c, _d;
    var att = entity.attributes;
    if ((0, ast_1.isLocalEntity)((_a = entity.superType) === null || _a === void 0 ? void 0 : _a.ref)) {
        att = entity.attributes.concat((_d = (_c = (_b = entity.superType) === null || _b === void 0 ? void 0 : _b.ref) === null || _c === void 0 ? void 0 : _c.attributes) !== null && _d !== void 0 ? _d : []);
    }
    //var relation = entity.relations.concat(entity.superType?.ref?.relations ?? [] )
    //${relation.map(relation => `${relation.name}:${generateRelationSchemaGraphQL(relation)}`).join("\n")}
    return (0, langium_1.expandToStringWithNL) `
    type ${entity.name}{
        id: ID!
        ${att.map(atribute => `${atribute.name}:String!`).join("\n")}
    }
    `;
}
function generateInputTypeSchemaGraphQL(entity) {
    var _a, _b, _c, _d;
    var att = entity.attributes;
    if ((0, ast_1.isLocalEntity)((_a = entity.superType) === null || _a === void 0 ? void 0 : _a.ref)) {
        att = entity.attributes.concat((_d = (_c = (_b = entity.superType) === null || _b === void 0 ? void 0 : _b.ref) === null || _c === void 0 ? void 0 : _c.attributes) !== null && _d !== void 0 ? _d : []);
    }
    //var relation = entity.relations.concat(entity.superType?.ref?.relations ?? [] )
    //${relation.map(relation => `${relation.name}ID:String`).join("\n")}
    return (0, langium_1.expandToStringWithNL) `
    input ${entity.name}Input{
      ${att.map(atribute => `${atribute.name}:String!`).join("\n")}
    }
    `;
}
function generateSchemaGraphQL(application) {
    const modules = application.abstractElements.filter(ast_1.isModule);
    const all_entities = modules.map(module => module.elements.filter(ast_1.isLocalEntity)).flat();
    return (0, langium_1.expandToStringWithNL) `
    
    ${all_entities.map(entity => entity.is_abstract ? "" : (0, langium_1.toString)(generateTypeSchemaGraphQL(entity))).join("\n")}
    
    ${all_entities.map(entity => entity.is_abstract ? "" : (0, langium_1.toString)(generateInputTypeSchemaGraphQL(entity))).join("\n")}
  
    type Query{
      ${all_entities.map(entity => entity.is_abstract ? "" : `findAll${entity.name}s:[${entity.name}]`).join("\n")}
      ${all_entities.map(entity => entity.is_abstract ? "" : `findByID${entity.name} (id: ID!):${entity.name}`).join("\n")}
    }
    
    type Mutation{
      ${all_entities.map(entity => entity.is_abstract ? "" : `create${entity.name}(input: ${entity.name}Input):${entity.name}`).join("\n")}
      ${all_entities.map(entity => entity.is_abstract ? "" : `delete${entity.name} (id: ID!):${entity.name}`).join("\n")}
      ${all_entities.map(entity => entity.is_abstract ? "" : `update${entity.name} (id: ID!, input: ${entity.name}Input):${entity.name}`).join("\n")}
    }`;
}
//# sourceMappingURL=graphql-generator.js.map