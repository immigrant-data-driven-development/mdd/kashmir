"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateConfigs = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const ast_1 = require("../../../language-server/generated/ast");
const langium_1 = require("langium");
const generator_utils_1 = require("../../util/generator-utils");
function generateConfigs(model, target_folder) {
    if (model.configuration) {
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'Dockerfile'), (0, langium_1.toString)(generateDockerFile()));
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'docker-compose-database.yml'), (0, langium_1.toString)(generateComposeDatabase(model.configuration)));
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'docker-compose.yml'), (0, langium_1.toString)(generateCompose(model.configuration)));
        const RESOURCE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/resources");
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'pom.xml'), (0, langium_1.toString)(generatePOMXML(model)));
        fs_1.default.writeFileSync(path_1.default.join(RESOURCE_PATH, 'logback.xml'), (0, langium_1.toString)(generatelogback()));
        fs_1.default.writeFileSync(path_1.default.join(RESOURCE_PATH, 'application.properties'), (0, langium_1.toString)(applicationProperties(model.configuration)));
    }
}
exports.generateConfigs = generateConfigs;
function generateDockerFile() {
    return (0, langium_1.expandToStringWithNL) `
  # Use an official Maven image as the base image
  FROM maven:3.8.4-openjdk-17-slim

  # Set the working directory inside the container
  WORKDIR /app

  # Copy the Maven project file and download dependencies
  COPY pom.xml .
  RUN mvn dependency:go-offline

  # Copy the application source code
  COPY src ./src

  # Build the application
  RUN mvn package

  # Expose the port that the application will run on
  EXPOSE 8080

  # Specify the command to run your application
  CMD ["mvn", "spring-boot:run"]

  `;
}
function applicationProperties(configuration) {
    return (0, langium_1.expandToStringWithNL) `
  spring.datasource.initialization-mode=always
  spring.datasource.url =  jdbc:postgresql://localhost:5432/${configuration.database_name}
  spring.datasource.username = postgres
  spring.datasource.password = postgres
  spring.datasource.platform= postgres
  #spring.jpa.hibernate.ddl-auto = update
  spring.jpa.hibernate.ddl-auto = create-drop
  
  spring.jpa.properties.javax.persistence.schema-generation.create-source=metadata
  spring.jpa.properties.javax.persistence.schema-generation.scripts.action=create-drop
  spring.jpa.properties.javax.persistence.schema-generation.scripts.drop-target=sql/${configuration.database_name.toLowerCase()}.sql
  spring.jpa.properties.javax.persistence.schema-generation.scripts.create-target=sql/${configuration.database_name.toLowerCase()}.sql

  logging.level.org.hibernate.SQL=DEBUG
  server.port=8081

  springdoc.swagger-ui.path=/
  springdoc.packagesToScan=${configuration.package_path}.*

  spring.graphql.graphiql.enabled: true
  spring.graphql.schema.locations=classpath:graphql/ 
  spring.graphql.schema.fileExtensions=.graphqls, .gqls
  `;
}
function generatelogback() {
    return (0, langium_1.expandToStringWithNL) `
  <?xml version="1.0" encoding="UTF-8"?>
  <configuration>
      <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
          <encoder>
              <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n
              </pattern>
          </encoder>
      </appender>

      <root level="INFO">
          <appender-ref ref="STDOUT" />
      </root>
  </configuration>
  `;
}
function generatePOMXML(application) {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    return (0, langium_1.expandToStringWithNL) `
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.1.0</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>${(_a = application.configuration) === null || _a === void 0 ? void 0 : _a.package_path}.service.</groupId>
	<artifactId>${(_b = application.configuration) === null || _b === void 0 ? void 0 : _b.software_name}</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>${(_c = application.configuration) === null || _c === void 0 ? void 0 : _c.software_name}</name>
	<description>${(_d = application.configuration) === null || _d === void 0 ? void 0 : _d.about}</description>
	<properties>
		<java.version>17</java.version>
        <start-class>${(_e = application.configuration) === null || _e === void 0 ? void 0 : _e.package_path}.service.${(_f = application.configuration) === null || _f === void 0 ? void 0 : _f.software_name}.application.Application</start-class>
	</properties>

  <repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/#change/-/packages/maven</url>
  </repository>
</repositories>


	<dependencies>

    <dependency>
      <groupId>${(_g = application.configuration) === null || _g === void 0 ? void 0 : _g.package_path}.entity</groupId>
      <artifactId>${(_h = application.configuration) === null || _h === void 0 ? void 0 : _h.software_name}</artifactId>
      <version>0.0.1-SNAPSHOT</version>
    </dependency>

    ${application.abstractElements.filter(ast_1.isModuleImport).map(moduleImport => generateOntologyDependency(moduleImport)).join("\n")}

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-rest</artifactId>
		</dependency>
    <dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>
    <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <scope>runtime</scope>
    </dependency>

    <dependency>
        <groupId>org.springframework.data</groupId>
        <artifactId>spring-data-commons</artifactId>
    </dependency>


    <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
  
<dependency>
    <groupId>org.springdoc</groupId>
    <artifactId>springdoc-openapi-starter-common</artifactId>
    <version>2.0.2</version>
</dependency>

<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-graphql</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

   <dependency>
      <groupId>org.springdoc</groupId>
      <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
      <version>2.0.2</version>
   </dependency>




	</dependencies>


	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
  `;
}
function generateOntologyDependency(moduleImported) {
    return (0, langium_1.expandToStringWithNL) `
  <dependency>
  <groupId>br.nemo.immigrant.ontology.entity</groupId>
  <artifactId>${moduleImported.ontology.toLowerCase()}</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
  `;
}
function generateCompose(configuration) {
    return (0, langium_1.expandToStringWithNL) `
  version: '3.9'
  services:
    ontology_service:
      container_name: ontology_${configuration.software_name}_service
      build: .
      image: registry.gitlab.com/immigrant-data-driven-development/services/${configuration.software_name}
      ports:
        - "8080:8080"
  networks: 
    default: 
      name: base-infrastrutrure
      external: true
  
  `;
}
function generateComposeDatabase(configuration) {
    var _a, _b;
    return (0, langium_1.expandToStringWithNL) `
    version: '3.7'

    services:
      postgres:
        image: postgres
        ports:
          - "5432:5432"
        restart: always
        environment:
          POSTGRES_PASSWORD: postgres
          POSTGRES_DB: ${(_b = (_a = configuration.database_name) !== null && _a !== void 0 ? _a : configuration.software_name) !== null && _b !== void 0 ? _b : 'KashmirDB'}
          POSTGRES_USER: postgres
        volumes:
          - ./data:/var/lib/postgresql
          - ./pg-initdb.d:/docker-entrypoint-initdb.d
  `;
}
//# sourceMappingURL=config-generator.js.map