"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateEnum = void 0;
const langium_1 = require("langium");
function generateEnum(enumx, package_name) {
    return (0, langium_1.expandToString) `
    package ${package_name}.models;
    
    public enum ${enumx.name} {
        ${enumx.attributes.map(a => `${a.name.toUpperCase()}`).join(",\n")}
    }
  `;
}
exports.generateEnum = generateEnum;
//# sourceMappingURL=enum-generator.js.map