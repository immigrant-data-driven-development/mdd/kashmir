"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateModules = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const ast_1 = require("../../language-server/generated/ast");
const generator_utils_1 = require("../util/generator-utils");
const relations_1 = require("../util/relations");
const langium_1 = require("langium");
const model_generator_1 = require("./module/model-generator");
const enum_generator_1 = require("./module/enum-generator");
function generateModules(model, target_folder) {
    var _a, _b;
    const package_path = (_b = (_a = model.configuration) === null || _a === void 0 ? void 0 : _a.package_path) !== null && _b !== void 0 ? _b : 'base';
    const modules = model.abstractElements.filter(ast_1.isModule);
    const all_entities = modules.map(module => module.elements.filter(ast_1.isLocalEntity)).flat();
    const relation_maps = (0, relations_1.processRelations)(all_entities);
    if (model.configuration) {
        const package_name_application = `${package_path}.application`;
        const APPLICATION_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", package_name_application.replaceAll(".", "/"));
        fs_1.default.writeFileSync(path_1.default.join(APPLICATION_PATH, `Application.java`), applicationGenerator(package_name_application, model.configuration));
    }
    for (const mod of modules) {
        const package_name = `${package_path}.${mod.name.toLowerCase()}`;
        const MODULE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", package_name.replaceAll(".", "/"));
        const REPOSITORIES_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'repositories');
        const MODELS_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'models');
        const supertype_classes = processSupertypes(mod);
        const mod_classes = mod.elements.filter(ast_1.isLocalEntity);
        for (const cls of mod_classes) {
            const class_name = cls.name;
            const { attributes, relations } = getAttrsAndRelations(cls, relation_maps);
            attributes;
            fs_1.default.writeFileSync(path_1.default.join(MODELS_PATH, `${class_name}.java`), (0, langium_1.toString)((0, model_generator_1.generateModel)(cls, supertype_classes.has(cls), relations, package_name)));
            if (!cls.is_abstract) {
                fs_1.default.writeFileSync(path_1.default.join(REPOSITORIES_PATH, `${class_name}Repository.java`), (0, langium_1.toString)(generateClassRepository(cls, package_name)));
            }
        }
        for (const enumx of mod.elements.filter(ast_1.isEnumX)) {
            fs_1.default.writeFileSync(path_1.default.join(MODELS_PATH, `${enumx.name}.java`), (0, enum_generator_1.generateEnum)(enumx, package_name));
        }
    }
}
exports.generateModules = generateModules;
function applicationGenerator(path_package, configuration) {
    return (0, langium_1.expandToStringWithNL) `
  package ${path_package};

  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.SpringBootApplication;
  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.SpringBootApplication;
  import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
  import org.springframework.boot.autoconfigure.*;
  import org.springframework.context.annotation.*;
  import org.springframework.boot.autoconfigure.domain.EntityScan;
  import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

  import io.swagger.v3.oas.annotations.OpenAPIDefinition;
  import io.swagger.v3.oas.annotations.info.Info;

  @SpringBootApplication
  @EnableAutoConfiguration
  @ComponentScan(basePackages = {"${path_package.replace("application", "")}.*"})
  @EntityScan(basePackages = {"${path_package.replace("application", "")}.*"})
  @EnableJpaRepositories(basePackages = {"${path_package.replace("application", "")}.*"})
  @OpenAPIDefinition(info = @Info(
    title = "${configuration.software_name}", 
    version = "1.0", 
    description = "${configuration.about}"))

  public class Application {

    public static void main(String[] args) {
      SpringApplication.run(Application.class, args);
    }
  }
  `;
}
/**
 * Dado um módulo, retorna todos as classes dele que são usadas como Superclasses
 */
function processSupertypes(mod) {
    var _a, _b;
    const set = new Set();
    for (const cls of mod.elements.filter(ast_1.isLocalEntity)) {
        if (((_a = cls.superType) === null || _a === void 0 ? void 0 : _a.ref) != null) {
            set.add((_b = cls.superType) === null || _b === void 0 ? void 0 : _b.ref);
        }
    }
    return set;
}
/**
 * Retorna todos os atributos e relações de uma Class, incluindo a de seus supertipos
 */
function getAttrsAndRelations(cls, relation_map) {
    var _a, _b, _c, _d;
    // Se tem superclasse, puxa os atributos e relações da superclasse
    if (((_a = cls.superType) === null || _a === void 0 ? void 0 : _a.ref) != null) {
        const parent = (_b = cls.superType) === null || _b === void 0 ? void 0 : _b.ref;
        const { attributes, relations } = getAttrsAndRelations(parent, relation_map);
        return {
            attributes: attributes.concat(cls.attributes),
            relations: relations.concat((_c = relation_map.get(cls)) !== null && _c !== void 0 ? _c : [])
        };
    }
    else {
        return {
            attributes: cls.attributes,
            relations: (_d = relation_map.get(cls)) !== null && _d !== void 0 ? _d : []
        };
    }
}
function generateClassRepository(cls, package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.repositories;

    import ${package_name}.models.${cls.name};

    import org.springframework.data.repository.PagingAndSortingRepository;
    import org.springframework.data.repository.ListCrudRepository;
    import org.springframework.data.rest.core.annotation.RestResource;
    import org.springframework.data.rest.core.annotation.RepositoryRestResource;

    @RepositoryRestResource(collectionResourceRel = "${cls.name.toLowerCase()}", path = "${cls.name.toLowerCase()}")
    public interface ${cls.name}Repository extends PagingAndSortingRepository<${cls.name}, Long>, ListCrudRepository<${cls.name}, Long> {
    
    }
  `;
}
//# sourceMappingURL=module-generator.js.map