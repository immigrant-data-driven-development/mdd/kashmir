import path from "path";
import { Application } from '../language-server/generated/ast';
import { generateMainMicronaut } from "./MicronautGenerator/main-generator";
import { generateMainSpringData } from "./SpringDataGenerator/main-generator";
import { generateDocumentation } from "./DocumentationGenerator/main-generator";

export function generateJavaScript(model: Application, filePath: string, destination: string | undefined): string {
    const final_destination = extractDestination(filePath, destination)
    if (model.configuration){
        console.log(model.configuration?.framework)
        if (model.configuration?.framework === 'micronaut'){
            generateMainMicronaut(model, final_destination)
        }
        else {
            
            generateMainSpringData(model, final_destination)
        }
        
    }
    

    generateDocumentation(model,final_destination)
    return final_destination
}

function extractDestination(filePath: string, destination?: string) : string {
    const path_ext = new RegExp(path.extname(filePath)+'$', 'g')
    filePath = filePath.replace(path_ext, '')
  
    return destination ?? path.join(path.dirname(filePath), "generated")
  }
