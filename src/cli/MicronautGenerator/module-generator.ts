import path from "path";
import fs from "fs";

import { Application, isLocalEntity, isModule, LocalEntity, Attribute, Module, isEnumX, Entity } from "../../language-server/generated/ast";
import { createPath } from "../util/generator-utils";
import { processRelations, RelationInfo } from "../util/relations";

import { expandToStringWithNL, Generated, toString } from "langium";
import { generateModel} from "./module/model-generator";
import { generateInputDTO, generateOutputDTO} from "./module/dtos-generator";
import { generateNotFoundException, generateNotFoundHandler,generateClassNotFoundException } from "./module/exception-generator";
import { generateController } from "./module/controller-generator";
import { generateEnum } from "./module/enum-generator";

export function generateModules(model: Application, target_folder: string) : void {
  
  const package_path  = model.configuration?.package_path ?? 'base'

  const modules =  model.abstractElements.filter(isModule);

  const all_entities = modules.map(module => module.elements.filter(isLocalEntity)).flat()

  const relation_maps = processRelations(all_entities)

  for(const mod of modules) {
    
    const package_name      = `${package_path}.${mod.name.toLowerCase()}`
    const MODULE_PATH       = createPath(target_folder, "src/main/java/", package_name.replaceAll(".","/"))
    const EXCEPTIONS_PATH   = createPath(MODULE_PATH, 'exceptions')
    const APPLICATIONS_PATH = createPath(MODULE_PATH, 'applications')
    const REPOSITORIES_PATH = createPath(MODULE_PATH, 'repositories')
    const CONTROLLERS_PATH  = createPath(MODULE_PATH, 'controllers')
    const MODELS_PATH       = createPath(MODULE_PATH, 'models')
    const DTOS_PATH         = createPath(MODULE_PATH, 'dtos')

    fs.writeFileSync(path.join(EXCEPTIONS_PATH, 'NotFoundException.java'), toString(generateNotFoundException(package_name)))
    fs.writeFileSync(path.join(EXCEPTIONS_PATH, 'NotFoundHandler.java'),   toString(generateNotFoundHandler(package_name)))
    
    
    const supertype_classes = processSupertypes(mod)

    const mod_classes = mod.elements.filter(isLocalEntity)
    for(const cls of mod_classes) {
      const class_name = cls.name
      const {attributes, relations} = getAttrsAndRelations(cls, relation_maps)

      fs.writeFileSync(path.join(MODELS_PATH,       `${class_name}.java`), toString(generateModel(cls, supertype_classes.has(cls), relations, package_name)))
      fs.writeFileSync(path.join(APPLICATIONS_PATH, `${class_name}Apl.java`), toString(generateApplication(cls, package_name)))
      fs.writeFileSync(path.join(DTOS_PATH,         `${class_name}InputDto.java`), toString(generateInputDTO(cls, attributes, relations, package_name)))
      fs.writeFileSync(path.join(DTOS_PATH,         `${class_name}OutputDto.java`), toString(generateOutputDTO(cls, attributes, relations, package_name)))
      fs.writeFileSync(path.join(CONTROLLERS_PATH,  `${class_name}Controller.java`), toString(generateController(cls, attributes, relations, package_name)))
      fs.writeFileSync(path.join(REPOSITORIES_PATH, `${class_name}Repository.java`), toString(generateClassRepository(cls, package_name)))
      fs.writeFileSync(path.join(EXCEPTIONS_PATH,   `${class_name}NotFoundException.java`), toString(generateClassNotFoundException(cls, package_name)))
      
    }

    for (const enumx of mod.elements.filter(isEnumX)){
      fs.writeFileSync(path.join(MODELS_PATH,`${enumx.name}.java`), generateEnum(enumx,package_name))
    }
  }
}

/**
 * Dado um módulo, retorna todos as classes dele que são usadas como Superclasses
 */
function processSupertypes(mod: Module) : Set<Entity| undefined> {
  const set: Set<Entity | undefined> = new Set()
  for(const cls of mod.elements.filter(isLocalEntity)) {
    
    if(cls.superType?.ref != null && isLocalEntity(cls)) {
      set.add(cls.superType?.ref)
    }
  }
  return set
}

/**
 * Retorna todos os atributos e relações de uma Class, incluindo a de seus supertipos
 */
function getAttrsAndRelations(cls: LocalEntity, relation_map: Map<LocalEntity, RelationInfo[]>) : {attributes: Attribute[], relations: RelationInfo[]} {
  // Se tem superclasse, puxa os atributos e relações da superclasse
  if(cls.superType?.ref != null && isLocalEntity(cls.superType?.ref)) {
    const parent = cls.superType?.ref
    const {attributes, relations} = getAttrsAndRelations(parent, relation_map)

    return {
      attributes: attributes.concat(cls.attributes),
      relations: relations.concat(relation_map.get(cls) ?? [])
    }
  } else {
    return {
      attributes: cls.attributes,
      relations: relation_map.get(cls) ?? []
    }
  }
}

function generateClassRepository(cls: LocalEntity, package_name: string) : Generated {
  return expandToStringWithNL`
    package ${package_name}.repositories;

    import ${package_name}.models.${cls.name};

    import java.util.UUID;
    import io.micronaut.data.annotation.Repository;
    import io.micronaut.data.jpa.repository.JpaRepository;

    @Repository
    public interface ${cls.name}Repository extends JpaRepository<${cls.name}, UUID>{

    }
  `
}

function generateApplication(cls: LocalEntity, package_name: string) : Generated {
  return expandToStringWithNL`
    package ${package_name}.applications;

    import ${package_name}.models.${cls.name};
    import ${package_name}.repositories.${cls.name}Repository;

    import java.util.List;
    import java.util.Optional;
    import java.util.UUID;
    import jakarta.inject.Singleton;
    import lombok.RequiredArgsConstructor;

    @Singleton
    @RequiredArgsConstructor
    public class ${cls.name}Apl {
        private final ${cls.name}Repository repo;

        public ${cls.name} save(${cls.name} p) {
            return this.repo.save(p);
        }

        public List<${cls.name}> findAll() {
            return this.repo.findAll();
        }

        public Optional<${cls.name}> findById(UUID id) {
            return this.repo.findById(id);
        }

        public ${cls.name} update(${cls.name} p) {
            return this.repo.update(p);
        }

        public void delete(${cls.name} p) {
            this.repo.delete(p);
        }
    }
  `
}
