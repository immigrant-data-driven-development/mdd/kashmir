import path from 'path'
import fs from 'fs'

import { Application,Configuration, isLocalEntity, isModule } from "../../../language-server/generated/ast";
import { expandToStringWithNL, Generated, toString } from 'langium';
import { createPath } from '../../util/generator-utils';

export function generateDebezium(model: Application, target_folder: string) {

    if (model.configuration){
        // criando a pasta que salva o SQL
        const SQL_PATH = createPath(target_folder, "sql")
        fs.writeFileSync(path.join(SQL_PATH, 'debezium.sql'), toString(generateDebeziumSQL(model)))

        const REGISTER_PATH = createPath(target_folder, "register")
        fs.writeFileSync(path.join(REGISTER_PATH, model.configuration.software_name.toLowerCase()+'-register.json'), toString(generateDebeziumRegister(model.configuration)))
    }
}

function generateDebeziumSQL(model:Application): Generated{
    return expandToStringWithNL`
    ${model.abstractElements.filter(isModule).map(module => module.elements.filter(isLocalEntity).map(entity => !entity.is_abstract? `ALTER TABLE public.${entity.name.toLowerCase()}  REPLICA IDENTITY FULL;`: undefined).join('\n')).join('\n')}  
    `
  }

  function generateDebeziumRegister (configuration: Configuration): Generated{

    return expandToStringWithNL`
    {
      "name": "${configuration.software_name.toLowerCase()}-connector",
      "config": {
          "connector.class": "io.debezium.connector.postgresql.PostgresConnector",
          "tasks.max": "1",
          "database.hostname": "db-pg",
          "database.port": "5432",
          "database.user": "postgres",
          "database.password": "postgres",
          "database.dbname" : "${configuration.software_name.toLowerCase()}",
          "topic.prefix": "ontology.${configuration.software_name.toLowerCase()}",
          "topic.partitions": 3,
          "schema.include.list": "public"
      }
  }
    
    `
  
  }
  