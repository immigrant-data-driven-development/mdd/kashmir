import { expandToString } from "langium";
import { EnumX } from "../../../language-server/generated/ast";


export function generateEnum(enumx: EnumX, package_name: string) : string {
  

  return expandToString`
    package ${package_name}.models;
    
    public enum ${enumx.name} {
        ${enumx.attributes.map(a => `${a.name.toUpperCase()}` ).join(",\n")}
    }
  `;
}