import fs from "fs";
import { Application } from "../../../language-server/generated/ast";
import { generateConfigs } from "./config-generator";
import { generateModules } from "./module-generator";
import {generateSchemaSQLHelper} from "./sql-generator";
import {generateDebezium} from "./debezium-generator"

export function generateEntiySpringData(model: Application, target_folder: string) : void {
  fs.mkdirSync(target_folder, {recursive:true})
  generateConfigs(model, target_folder);
  generateModules(model, target_folder);
  generateSchemaSQLHelper(model,target_folder);
  generateDebezium(model,target_folder)


}