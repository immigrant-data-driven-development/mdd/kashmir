import fs from "fs";
import { Application } from "../../../language-server/generated/ast";
import { generateConfigs } from "./config-generator";
import { generateModules } from "./module-generator";
import { generateGraphQL } from "./graphql-generator";

export function generateWebserviceSpringData(model: Application, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
    generateConfigs(model, target_folder);
    generateModules(model, target_folder);
    generateGraphQL(model, target_folder);


}