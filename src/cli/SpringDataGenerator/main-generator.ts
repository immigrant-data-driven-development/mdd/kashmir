import fs from "fs";
import { Application } from "../../language-server/generated/ast";
import { generateWebserviceSpringData } from "./webservice/webservice-main-generator";
import { generateEntiySpringData } from "./entity/entity-main-generator";

export function generateMainSpringData(model: Application, target_folder: string) : void {
  const target_folder_entity = target_folder+"/entity"
  const target_folder_webservice = target_folder+"/webservice"
  
  fs.mkdirSync(target_folder_entity, {recursive:true})
  fs.mkdirSync(target_folder_webservice, {recursive:true})

  generateWebserviceSpringData(model, target_folder_webservice)
  generateEntiySpringData(model, target_folder_entity)
 
}