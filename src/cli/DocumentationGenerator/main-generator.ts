import fs from "fs";
import { expandToStringWithNL } from "langium";
import { createPath, base_ident } from '../util/generator-utils'
import { Application,Configuration, isEnumX, Module, isModule, LocalEntity, isLocalEntity, Relation, isOneToOne, isManyToMany, isManyToOne, isImportedEntity
 } from "../../language-server/generated/ast";
import path from 'path'

const ident = base_ident

export function generateDocumentation(model: Application, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
  
    if (model.configuration){
        fs.writeFileSync(path.join(target_folder, 'README.md'),createProjectReadme(model.configuration))
        fs.writeFileSync(path.join(target_folder, '.gitlab-ci.yml'),createGitlab())
    }

    const DOCS_PATH = createPath(target_folder, "docs")
    const modules = model.abstractElements.filter(isModule)
    
    fs.writeFileSync(path.join(DOCS_PATH, "README.md"), generalREADME(modules))
    fs.writeFileSync(path.join(DOCS_PATH, "packagediagram.puml"), createPackageDiagram(modules, model.configuration?.software_name))

    for (const m of modules) {
        const MODULE_PATH = createPath(DOCS_PATH, m.name.toLowerCase())
        fs.writeFileSync(path.join(MODULE_PATH, "/README.md"), moduleREADME(m))
        fs.writeFileSync(path.join(MODULE_PATH, "/classdiagram.puml"), createClassDiagram(m))
    }

  
}

function createGitlab():string{
    return expandToStringWithNL`
    variables:
    CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest

    stages:
    - build-entity
    - build-webservice
    - release-webservice

    maven-build:
    stage: build-entity
    image: maven:latest
    script: 
        - cd entity
        - mvn deploy -s settings.xml -DskipTests

    build-webservice:
    stage: build-webservice
    image: docker:20.10.16
    services:
        - docker:20.10.16-dind
    before_script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    script:
        - cd webservice
        - docker build --pull -t $CONTAINER_TEST_IMAGE .
        - docker push $CONTAINER_TEST_IMAGE

    release-master-webservice:
    stage: release-webservice
    image: docker:20.10.16
    services:
        - docker:20.10.16-dind
    before_script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    script:
        - cd webservice
        - docker pull $CONTAINER_TEST_IMAGE
        - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
        - docker push $CONTAINER_RELEASE_IMAGE
    only:
        - main

    release-dev-webservice:
    stage: release-webservice
    script:
        - cd webservice
        - docker pull $CONTAINER_TEST_IMAGE
        - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_TEST_IMAGE
        - docker push $CONTAINER_TEST_IMAGE
    only:
        - dev

    `
}

function createPackageDiagram(modules: Module[], name?: string) : string {
    return expandToStringWithNL`
    @startuml ${name ?? ''}
    ${modules.flatMap(m => `namespace ${m.name}{}`).join('\n')}
    @enduml`
}


function createClassDiagram(m: Module) : string {
    const enums = m.elements.filter(isEnumX)
    const entities = m.elements.filter(isLocalEntity)

    return expandToStringWithNL`
    @startuml ${m.name}
        ${enums.flatMap(e =>[`enum ${e.name} { \n ${e.attributes.map(a => `${ident}${a.name}`).join(`\n`)}\n}`]).join(`\n`)}
        ${entities.map (e => entityClassDiagram(e, m)).join(`\n`)}
    @enduml`    

}

function entityClassDiagram(e: LocalEntity, m: Module) : string {
    const lines = [
        `class ${e.name} ${e.superType ? isImportedEntity (e.superType.ref) ? `<< ${e.superType.ref.name}>>`: `` : ``}{`,
        ...e.attributes.map(a =>
            `${a.type}: ${a.name}`
        ),
        ``,
        ...e.relations.filter(r => !isManyToMany(r)).map(r =>
            `${r.type.ref?.name}: ${r.name.toLowerCase()}`
        ),
        `}`,
        e.superType?.ref ? `\n${e.superType.ref.name} <|-- ${e.name}\n` : '',
        e.enumentityatributes.map(a =>
            `${e.name} "1" -- "1" ${a.type.ref?.name} : ${a.name.toLowerCase()}>`,
        ),
        ...e.relations.filter(r => !isManyToOne(r)).map(r => relationDiagram(r, e, m)),
        ``
    ]

    return lines.join('\n')
}

function relationDiagram(r: Relation, e: LocalEntity, m: Module) : string {
    // Cardinalidades
    const tgt_card = isOneToOne(r)   ? "1" : "0..*"
    const src_card = isManyToMany(r) ? "0..*" : "1"
    // Módulo de origem da entidade destino
    const origin_module = r.type.ref?.$container.name.toLowerCase() !== m.name.toLowerCase() ?
        `${r.type.ref?.$container.name}.` :
        ""

    return `${e.name} "${src_card}" -- "${tgt_card}" ${origin_module}${r.type.ref?.name} : ${r.name.toLowerCase()} >`
}


function moduleREADME(m: Module) : string {
    const lines = [
        `# 📕Documentation: ${m.name}`,
        ``,
        `${m.description ?? ''}`,
        ``,
        `## 🌀 Package's Data Model`,
        ``,
        `![Domain Diagram](classdiagram.png)`,
        ``,
        `### ⚡Entities`,
        ``,
        ...m.elements.filter(isLocalEntity).map(e =>
            `* **${e.name}** : ${e.description ?? '-'}`
        ),
        ``
    ]

    return lines.join('\n')
}


function generalREADME(modules: Module[]) : string {
    return expandToStringWithNL`
    # 📕Documentation

    ## 🌀 Project's Package Model
    ![Domain Diagram](packagediagram.png)

    ### 📲 Modules
    ${modules.map(m => `* **[${m.name}](./${m.name.toLocaleLowerCase()}/)** :${m.description ?? '-'}`).join("\n")}

    `
}

function stackREADME (stack: string): string {
    if (stack == "springboot"){
        return expandToStringWithNL`
        1. Spring Boot 3.0
        2. Spring Data Rest
        3. Spring GraphQL
        `
    } 
    return expandToStringWithNL`
    1. Micronaut 3.8.2    
    `

}

function createProjectReadme(configuration: Configuration): string{
    return expandToStringWithNL`
    # ${configuration.software_name}
    ## 🚀 Goal
    ${configuration.about}

    ## 📕 Domain Documentation
    
    Domain documentation can be found [here](./docs/README.md)

    ## ⚙️ Requirements

    1. Postgresql
    2. Java 17
    3. Maven

    ## ⚙️ Stack 
    ${stackREADME(configuration.framework)}

    ## 🔧 Install

    1) Create a database with name ${configuration.software_name} with **CREATE DATABASE ${configuration.software_name}**.
    2) Run the command to start the webservice and create table of database:

    \`\`\`bash
    mvn Spring-boot:run 
    \`\`\`

    ## Debezium

    Go to folder named *register* and performs following command to register in debezium:

    \`\`\`bash
    curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-sro.json
    \`\`\`

    To delete, uses:

    \`\`\`bash
    curl -i -X DELETE localhost:8083/connectors/sro-connector/
    \`\`\`
        
    
    ## 🔧 Usage

    * Access [http://localhost:${configuration.service_port}](http://localhost:${configuration.service_port}) to see Swagger 
    * Acess [http://localhost:${configuration.service_port}/grapiql](http://localhost:${configuration.service_port}/grapiql) to see Graphql.

    ## ✒️ Team
    
    * **[${configuration.author}](${configuration.author_email})**
    
    ## 📕 Literature

    `
}