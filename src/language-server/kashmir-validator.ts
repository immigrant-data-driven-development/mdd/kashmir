import { ValidationChecks } from 'langium';
import { KashmirAstType } from './generated/ast';
import type { KashmirServices } from './kashmir-module';

/**
 * Register custom validation checks.
 */
export function registerValidationChecks(services: KashmirServices) {
    const registry = services.validation.ValidationRegistry;
    const validator = services.validation.KashmirValidator;
    const checks: ValidationChecks<KashmirAstType> = {
        
    };
    registry.register(checks, validator);
}

/**
 * Implementation of custom validations.
 */
export class KashmirValidator {

    

}
